# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala |9H |
| jak se mi to podařilo rozplánovat | Nepodařilo |
| design zapojení |https://gitlab.spseplzen.cz/chmelirp/vlastni-shield/-/blob/main/dokumentace/foto/Fritzing.JPG |
| proč jsem zvolil tento design | dle mě nejpraktičtější|
| zapojení  |
| z jakých součástí se zapojení skládá |enkoder, rgba stripe, 4x odpory, vodiče, 2x rgb led, teploměr, shield|
| realizace |  |
| nápad, v jakém produktu vše propojit dohromady|do výuky|
| co se mi povedlo | udělat to |
| co se mi nepovedlo/příště bych udělal/a jinak | Lépe rozplánovat.|
| zhodnocení celé tvorby | 7/10|